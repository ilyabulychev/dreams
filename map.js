import map from './map.json' assert { type: "json" };

 // Create the honeycomb pattern
 var svg = document.getElementById("honeycomb");
 var width = svg.getAttribute("width");
 var height = svg.getAttribute("height");

 var size = 50;
 const spacing = size / 10; 
 var xOffset = size * 3 + spacing * 2;
 var yOffset = size * Math.cos(Math.PI/6)  + spacing;

 var numRows = Math.ceil(height / yOffset) + 1;
 var numCols = Math.ceil(width / xOffset) + 1;

 const matrix = map;

 for (var row = 0; row < matrix.length; row++) {
   for (var col = 0; col < matrix[row].length; col++) {
     var x = col * xOffset + (row % 2) * (xOffset / 2);
     var y = row * yOffset;

     createPolygon(x, y, size, matrix[row][col]?.color);
   }
 }

 function createPolygon(centerX, centerY, size, color) {

     var hexagon = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
     hexagon.setAttribute("points", getHexagonPoints(centerX, centerY, size));
     hexagon.setAttribute("fill", color);
     svg.appendChild(hexagon);
 }

 function getHexagonPoints(centerX, centerY, size) {

     const points = [];

     for (let angle = 0; angle < 360; angle += 60) {
         const radians = (Math.PI / 180) * angle;
         const x = Math.floor(centerX + size * Math.cos(radians));
         const y = Math.floor(centerY + size * Math.sin(radians));
         points.push([x, y]);
     }
     return points;
 }