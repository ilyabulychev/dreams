let PAGE = 'index';

let landingPages = ['landing-page', 'section-page-1', 'section-page-2', 'section-page-3'];
let specialPages = ['forum-page', 'login-page', 'map-page', 'journal-page'];

init();

function showPage(page) {
    switch(page) {
        case 'login':
            loadPage('login-page');
            break;
        case 'forum':
            loadPage('forum-page');
            break;
        case 'map':
            loadPage('map-page');
            break;
        case 'journal':
            loadPage('journal-page');
            break;
    }
}

function loadPage(page) {
    PAGE = page;
    hideLandingPages();
    let element = document.getElementsByClassName(page)[0];
    element.style.removeProperty('display');

}

function init() {
    specialPages.forEach(page => {
        let _page = document.getElementsByClassName(page)[0];
        _page.style.display = 'none';
    });
}

function hideLandingPages() {
    landingPages.forEach(page => {
        let _page = document.getElementsByClassName(page)[0];
        _page.style.display = 'none';
    });

    specialPages.forEach(page => {
        let _page = document.getElementsByClassName(page)[0];
        _page.style.display = 'none';
    });
}